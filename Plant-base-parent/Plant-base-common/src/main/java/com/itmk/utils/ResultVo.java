package com.itmk.utils;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data //主动生成get和set
@AllArgsConstructor
public class ResultVo<T> {
    private String msg;
    private int code;
    private T data;
}